# A deep configuration file for HoneySearch.
#
# Adam P. Goucher, 2016-12-29

# Use a block as the initial seed:
addseed oo$oo!

# Keep targets if they're either small (2), interesting (4), or sparse (16):
iflags 22

# We're uninterested in common objects:
boring xs4_33
boring xp2_7
boring xs6_696
boring xq4_153
boring xs7_2596
boring xs5_253
boring xs6_356
boring xs4_252
boring xs8_6996
boring xs7_25ac
boring xp2_7e
boring xs12_g8o653z11
boring xp2_318c
boring xs6_25a4
boring xs14_g88m952z121
boring xs8_69ic

# Ensure patterns stabilise within 512 generations:
timelimit 512

# Parallelise on 32 cores with 750 MB for each core:
threadcount 32
threadmem 750

# Bombard with three gliders, ensuring new targets fit into 48 x 48:
innersize 48
bombard 3

# Bombard with another glider, ensuring new targets fit into 40 x 40:
innersize 40
bombard 1

# Bombard with another glider, ensuring new targets fit into 32 x 32:
innersize 32
bombard 1

# Bombard with a sixth glider:
outersize 40
innersize 24
bombard 1

# Bombard with an seventh glider:
outersize 32
innersize 16
bombard 1

# Eliminate some more common objects:
boring xs14_69bqic
boring xs8_25ak8
boring xs8_35ac
boring xs10_g8o652z01
boring xs14_g88b96z123
boring xs16_g88m996z1221
boring xs9_178ko
boring xs11_g8o652z11
boring xs9_4aar
boring xs10_35ako
boring xs9_25ako

# Bombard with an eighth glider:
outersize 48
innersize 32
iflags 1048
bombard 1

# Bombard with a ninth glider:
outersize 40
bombard 1

# Bombard with a tenth glider, producing only sparse targets:
iflags 16
bombard 1

# Bombard with an eleventh glider, producing only individual objects:
iflags 4096
bombard 1

# Let the search broaden again:
outersize 48
innersize 32
iflags 22
bombard 4
iflags 1048
bombard 2
iflags 16
bombard 1
iflags 4096
bombard 1

# Bombard with a 20th glider, producing no new objects:
iflags 0
bombard 1

# Increase the number of threads:
threadcount 71

# Print all indecomposable block-to-(X+Y) conversions to stdout:
diterm 2
term 0
traverse 10

# Print all indecomposable X-to-Y conversions to stdout:
diterm 1
term 1
traverse 11

# Thank you for using HoneySearch; have a wonderful day!
