# A simple configuration file for HoneySearch.
#
# Adam P. Goucher, 2016-12-23

safety on

# Use blocks, blinkers, tubs, and beehives:
addseed oo$oo!
# addseed 3o!
# addseed bo$obo$bo!
# addseed boo$obbo$boo!

# We're uninterested in common objects:
boring xs4_33
boring xp2_7
boring xs6_696
boring xq4_153
boring xs7_2596
boring xs5_253
boring xs6_356
boring xs4_252
boring xs8_6996
boring xs7_25ac
boring xp2_7e
boring xs12_g8o653z11
boring xp2_318c
boring xs6_25a4
boring xs14_g88m952z121
boring xs8_69ic

# Ensure patterns stabilise within 512 generations:
timelimit 512

# Parallelise on 32 cores with 250 MB for each core:
threadcount 32
threadmem 250

# Bombard with five gliders, ensuring new targets fit into 32 x 32:
innersize 32
bombard 4

# Bombard with a final glider, producing no new targets:
iflags 0
bombard 1

print cat

# Print the transitions to stdout:
print minimal

# threadcount 1

term 0
diterm 1
traverse 8
print atomic

# assemble 20

# Thank you for using HoneySearch; have a wonderful day!
