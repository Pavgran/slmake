#pragma once

#include "../lifelib/pattern2.h"
#include "../lifelib/spantree.h"
#include "../lifelib/classifier.h"
#include "../lifelib/ssplit.h"
#include <set>
#include <cstdlib>
#include <sstream>
#include <algorithm>

/*
* Functionality for constructing constellations of still-lifes.
*/

namespace apg {


    double stlength(pattern stell, classifier &cfier) {

        bitworld env = (stell + stell[1]).flatlayer(0);
        bitworld live = stell.flatlayer(0);

        std::vector<bitworld> clusters = cfier.getclusters(live, env, false);
        std::vector<coords64> ccentres;

        for (uint64_t i = 0; i < clusters.size(); i++) {

            // Get bounding box:
            int64_t bbox[4] = {0};
            clusters[i].getbbox(bbox);
            int64_t mx2 = bbox[0] * 2 + bbox[2] - 1;
            int64_t my2 = bbox[1] * 2 + bbox[3] - 1;
            coords64 m2(mx2, my2);
            ccentres.push_back(m2);

        }

        return 0.5 * spanning_tree_length(ccentres);

    }

    pattern diagonalise(pattern inp) {
        /*
        * Create a diagonal line longer than the diameter of a pattern.
        */

        int64_t bbox[4] = {0};
        inp.getrect(bbox);

        pattern diagonal(inp.getlab(), "o$bo$2bo$3bo$4bo$5bo$6bo$7bo$8bo$9bo$10bo$11bo$12bo$13bo$14bo$15bo!", inp.getrule());
        for (uint64_t i = 4; i < 64; i++) {
            if (((1 << i) >= bbox[2]) && ((1 << i) >= bbox[3])) { break; }
            diagonal += diagonal(1 << i, 1 << i);
        }

        return diagonal;
    }

    std::vector<pattern> dimerise(pattern stell, classifier &cfier, pattern sword) {
        /*
        * Find promising still-life pairs and individual still-lifes
        * which the compiler has a good chance of being able to build.
        */

        std::vector<pattern> dimers;
        std::vector<std::vector<pattern> > sdimers;

        bitworld env = (stell + stell[1]).flatlayer(0);
        bitworld live = stell.flatlayer(0);

        std::vector<bitworld> clusters = cfier.getclusters(live, env, true);
        std::vector<coords64> ccentres;

        for (uint64_t i = 0; i < clusters.size(); i++) {

            // Get bounding box:
            int64_t bbox[4] = {0};
            clusters[i].getbbox(bbox);
            int64_t mx2 = bbox[0] * 2 + bbox[2] - 1;
            int64_t my2 = bbox[1] * 2 + bbox[3] - 1;
            coords64 m2(mx2, my2);
            ccentres.push_back(m2);

        }

        std::vector<edge64> edgelist = spanning_graph(ccentres);
        lifetree_abstract<uint32_t>* lab = stell.getlab();

        std::set<edge64> edgedump;

        // Include dimers:
        for (uint64_t i = 0; i < edgelist.size(); i++) {
            uint64_t idx = (edgelist[i].first < edgelist[i].second) ? edgelist[i].first : edgelist[i].second;
            uint64_t idy = (edgelist[i].first > edgelist[i].second) ? edgelist[i].first : edgelist[i].second;
            bitworld bw = clusters[edgelist[i].first];
            bw += clusters[edgelist[i].second];
            pattern dimer(lab, lab->demorton(bw, 1), stell.getrule());
            edge64 newedge(idx, idy);
            if (edgedump.count(newedge) == 0) {
                dimers.push_back(dimer);
                edgedump.insert(newedge);
            }
        }

        // Include monomers:
        for (uint64_t i = 0; i < clusters.size(); i++) {
            pattern monomer(lab, lab->demorton(clusters[i], 1), stell.getrule());
            dimers.push_back(monomer);
        }

        // std::cout << "Filtering " << dimers.size() << " dimers/monomers..." << std::endl;
        if (sword.empty()) {
            pattern diagonal = diagonalise(stell + stell[1]);
            sword = pattern(lab, "3o$4o$6o$b5o$2b4o$2b4o!", stell.getrule());
            // sword += sword(40, 40).convolve(sword).convolve(sword);
            sword = sword.convolve(diagonal);
            sword = sword.convolve(pattern(lab, "o$b2o$bo!", stell.getrule()));
        }

        // Filter dimers/monomers:
        for (uint64_t i = 0; i < dimers.size(); i++) {
            pattern dimer = dimers[i];
            pattern remainder = stell - dimer;
            if ((dimer.convolve(sword) & remainder).empty()) {
                uint64_t n = (dimer + dimer[1]).popcount((1 << 30) + 3);
                if (sdimers.size() <= n) { sdimers.resize(n + 1); }
                sdimers[n].push_back(dimer);
            }
        }
        dimers.clear();

        if (sdimers.size()) {
            for (uint64_t i = sdimers.size() - 1; i > 0; i--) {
                for (uint64_t j = 0; j < sdimers[i].size(); j++) {
                    dimers.push_back(sdimers[i][j]);
                }
            }
        }

        return dimers;
    }

    template <typename T> struct cgsalvo {

        std::vector<std::pair<T, char> > gliders;
        T dx;
        T dy;
        bool age;
        bool transpose;

        void glidermatch(pattern pat) {

            std::map<std::pair<int64_t, int64_t>, char> gmap;
            std::vector<pattern> matches;

            matches.push_back(pat.match(pattern(pat.getlab(), "3o$o$bo!", pat.getrule())));
            matches.push_back(pat.match(pattern(pat.getlab(), "boo$oo$bbo!", pat.getrule())));
            matches.push_back(pat.match(pattern(pat.getlab(), "oo$obo$o!", pat.getrule())));
            matches.push_back(pat.match(pattern(pat.getlab(), "bo$oo$obo!", pat.getrule())));

            int64_t bbox[4] = {0};

            for (uint64_t i = 0; i < 4; i++) {
                bitworld bw = matches[i].flatlayer(0);

                while (bw.population()) {
                    bitworld onecell = bw.get1cell();
                    bw -= onecell;
                    onecell.getbbox(bbox);
                    int64_t lane = (bbox[0] - bbox[1]) + ((i & 2) ? -1 : 0);
                    char parity = (i & 1) ? 'O' : 'E';
                    gmap[std::pair<int64_t, int64_t>(bbox[0] + bbox[1], lane)] = parity;
                }
            }

            for (auto it = gmap.begin(); it != gmap.end(); ++it) {
                T lane = it->first.second;
                char parity = it->second;
                gliders.push_back(std::pair<T, char>(lane, parity));
            }
        }

        pattern frompattern(pattern pat) {

            pattern block(pat.getlab(), "oo$oo!", pat.getrule());
            pattern antiblock = block.convolve(block).convolve(block).shift(-1, -1) - block;

            pattern blockloc = pat.match(block, antiblock);
            // std::cout << blockloc.popcount((1 << 30) + 3) << std::endl;
            int64_t bbox[4] = {0};
            blockloc.getrect(bbox);

            pattern centred = pat(-bbox[0], -bbox[1]);
            glidermatch(centred);
            pattern fin = centred[1 << 20];
            fin.getrect(bbox);
            dx = bbox[0];
            dy = bbox[1];
            age = 0;
            transpose = 0;

            fin = fin(-bbox[0], -bbox[1]);
            return fin;

        }

        void fromline(std::string line) {
            std::vector<std::string> colons = string_split(line, ':');
            if (colons.size() >= 3) {

                std::string t = colons[colons.size() - 2];
                std::vector<std::string> gstrings = string_split(t, ' ');
                for (uint64_t i = 0; i < gstrings.size(); i++) {
                    std::string g = gstrings[i];
                    if (g != "") {
                        char lastchar = g[g.length() - 1];
                        if ((lastchar == 'E') || (lastchar == 'O')) {
                            T j = (std::stoll(g.substr(0, g.length() - 1)) - 1) / 2;
                            gliders.push_back(std::pair<T, char>(j, lastchar));
                        }
                    }
                }

                std::string s = colons[colons.size() - 3];
                std::replace(s.begin(), s.end(), '(', ' ');
                std::replace(s.begin(), s.end(), ')', ' ');
                std::replace(s.begin(), s.end(), ',', ' ');
                std::vector<std::string> hstrings = string_split(s, ' ');
                std::vector<std::string> hstrings2;
                for (uint64_t i = 0; i < hstrings.size(); i++) {
                    std::string h = hstrings[i];
                    if (h != "") { hstrings2.push_back(h); }
                }
                if (hstrings2.size() == 3) {
                    dx = std::stoll(hstrings2[0]);
                    dy = std::stoll(hstrings2[1]);
                    transpose = (hstrings2[2] == "T");
                }

                std::string r = colons[colons.size() - 1];
                if (r.find("o") != std::string::npos) { age = 1; }
                if (r.find("e") != std::string::npos) { age = 0; }

            }
        }

    };

    struct cgfile {
        std::map<std::string, std::vector<std::vector<cgsalvo<int16_t> > > > sdata;
        std::string source;
        std::set<std::string> btargets;

        void digestmc(std::string filename, lifetree_abstract<uint32_t> *lab) {

            pattern fin(lab, filename);
            cgsalvo<int16_t> cg;
            pattern ptarg = cg.frompattern(fin);
            std::string target = ptarg._string32();
            if (cg.gliders.size() != 0) {
                if (sdata[target].size() <= cg.gliders.size()) {
                    sdata[target].resize(cg.gliders.size() + 1);
                }
                sdata[target][cg.gliders.size()].push_back(cg);
            }
            btargets.insert(target);
            btargets.insert(ptarg.transpose()._string32());
            source = pattern(lab, "oo$oo!", "b3s23")._string32();
            std::cout << " -- " << filename << " successfully read." << std::endl;

        }

        void readfile(std::string filename, lifetree_abstract<uint32_t> *lab, std::string rule) {
            std::ifstream f(filename);
            std::string line;
            std::string rlesofar;
            std::string target;
            bool readingsrc = false;

            if (!f.good()) { return; }

            std::cout << "Reading file " << filename << "..." << std::endl;
            while (std::getline(f, line)) {
                if (line.empty()) { continue; }
                char c = line[0];

                if ((c == ' ') || (c == '*')) {
                    if (rlesofar != "") { rlesofar += "$"; }
                    rlesofar += line;
                } else if (rlesofar != "") {
                    std::replace( rlesofar.begin(), rlesofar.end(), ' ', 'b');
                    std::replace( rlesofar.begin(), rlesofar.end(), '*', 'o');
                    rlesofar += "!";
                    // std::cout << rlesofar << std::endl;
                    pattern p(lab, rlesofar, rule);
                    if (readingsrc) { source = p._string32(); } else { target = p._string32(); }
                    rlesofar = "";
                }

                if (c == '-') {
                    if (line.find("Source") != std::string::npos) {
                        readingsrc = true;
                    } else if (line.find("Target") != std::string::npos) {
                        readingsrc = false;
                    }
                } else if (rlesofar == "") {
                    cgsalvo<int16_t> cg;
                    cg.fromline(line);
                    if (cg.gliders.size() != 0) {
                        if (sdata[target].size() <= cg.gliders.size()) {
                            sdata[target].resize(cg.gliders.size() + 1);
                        }
                        sdata[target][cg.gliders.size()].push_back(cg);
                    }
                }
            }
            std::cout << "..." << filename << " successfully read." << std::endl;
        }
    };

    struct cghq {
        /*
        * Collection of slow-salvo recipes
        */

        std::map<std::string, cgfile> cgfiles;
        std::string rule;
        std::string datadir;
        lifetree_abstract<uint32_t> *lab;

        uint64_t assertfile(std::string filename) {
            if (cgfiles.count(filename) == 0) {
                cgfiles[filename].readfile(filename, lab, rule);
            }
            return cgfiles[filename].sdata.size();
        }

        void assertbespoke(std::string dirname) {
            if (cgfiles.count(dirname) == 0) {
                std::ifstream f(dirname + "/filelist.txt");
                std::string line;
                while (std::getline(f, line)) {
                    std::string filename = dirname + "/" + line;
                    cgfiles[dirname].digestmc(filename, lab);
                }
            }
        }

        pattern precurse(pattern orig, classifier &cfier, int state, int initbail, int maxbail, uint32_t lastbail, int64_t *ideal) {

            lab = orig.getlab();
            rule = orig.getrule();

            assertbespoke(datadir+"bespoke");

            pattern stell = orig & orig[2] & orig[4] & orig[6] & orig[8];
            pattern exsalvo = orig - stell;
            pattern diagonal = diagonalise(stell + stell[1]);
            pattern smallblock(lab, "2o$2o!", rule);
            pattern bigblock(lab, "4o$4o$4o$4o!", rule);

            pattern sword(lab, "", rule);

            if (initbail >= 16) {
                sword = diagonal;
                if (initbail < 64) {
                    sword = sword.convolve(smallblock);
                } else {
                    sword = pattern(lab, "o!", rule);
                }
            }

            std::vector<pattern> xdimers = dimerise(stell, cfier, sword);

            sword = diagonal.convolve(bigblock);

            std::vector<pattern> dimers;
            pattern avoid(lab, "", rule);

            pattern unavoid(lab, "", rule);
            /*
            if (ideal != 0) {
                unavoid += pattern(lab, "oo$oo!", rule).shift(ideal[0], ideal[1]);
            }
            */

            for (auto it = cgfiles[datadir+"bespoke"].btargets.begin(); it != cgfiles[datadir+"bespoke"].btargets.end(); ++it) {
                pattern sterm(lab, lab->_string32(*it), rule);
                bitworld bw = stell.match(sterm).flatlayer(0);
                while (bw.population()) {
                    int64_t bbox[4] = {0};
                    bitworld onecell = bw.get1cell();
                    bw -= onecell;
                    onecell.getbbox(bbox);
                    pattern subset = sterm(bbox[0], bbox[1]);
                    avoid += subset;
                    dimers.push_back(subset);
                }
            }

            double stelllength = stlength(stell + unavoid - avoid, cfier);
            uint64_t nbespoke = dimers.size();

            for (uint64_t i = 0; i < xdimers.size(); i++) {
                if ((xdimers[i] & avoid).empty()) {
                    dimers.push_back(xdimers[i]);
                }
            }

            uint64_t smallobj = 0;

            if (state != 0) {
                std::cout << "Obtained " << dimers.size() << " dimers/monomers";
                if (nbespoke) { std::cout << " (including " << nbespoke << " bespoke objects)"; }
                std::cout << "." << std::endl;

                bitworld env = (stell + stell[1]).flatlayer(0);
                bitworld live = stell.flatlayer(0);
                std::vector<bitworld> clusters = cfier.getclusters(live, env, true);
                for (uint64_t i = 0; i < clusters.size(); i++) {
                    if (clusters[i].population() < 24) { smallobj += 1; }
                }
                std::cout << smallobj << " objects of < 24 cells." << std::endl;
            }
            pattern eglider(lab, "3o$o$bo!", rule);
            pattern oglider(lab, "b2o$2o$2bo!", rule);

            // Display dimers:
            for (uint64_t i = 0; i < dimers.size(); i++) {
                pattern dimer = dimers[i];
                bitworld live = dimer.flatlayer(0);
                bitworld env = (dimer + dimer[1]).flatlayer(0);
                std::map<std::string, int64_t> counts = cfier.census(live, env);

                std::ostringstream ss;
                uint64_t totobj = 0;
                for (auto it = counts.begin(); it != counts.end(); ++it) {
                    if (totobj != 0) { ss << "__"; }
                    if (it->second != 0) {
                        ss << it->first;
                        if (it->second != 1) { ss << "(" << it->second << ")"; }
                    }
                    totobj += it->second;
                }

                std::string lexrepr = ss.str();
                std::vector<std::string> prefices;

                prefices.push_back("bespoke");

                bool oneblock = false;

                if (lexrepr == "xs4_33") {
                    if (state == 0) { continue; }
                    if (smallobj == 1) { oneblock = true; }
                    prefices.push_back("longmove");
                } else if ((lexrepr == "xs6_696") || (lexrepr == "xs4_252")) {
                    prefices.push_back("longmove");
                    prefices.push_back("edgy/xs4_33");
                } else {
                    prefices.push_back("edgy/xs4_33");
                    if (totobj == 1) {
                        prefices.push_back("edgy/xs6_696");
                        prefices.push_back("edgy/xs4_252");
                    }
                }

                uint64_t bdiff = 0;
                if (oneblock) {
                    if (ideal != 0) {
                        int64_t bbox2[4] = {0};
                        dimer.getrect(bbox2);
                        bbox2[0] -= ideal[0];
                        bbox2[1] -= ideal[1];
                        bdiff = (bbox2[0] * bbox2[0]) + (bbox2[1] * bbox2[1]);
                    }

                    if (bdiff == 0) {
                        std::cout << "\033[32;1mAlgorithm terminated with single block.\033[0m" << std::endl;
                        return orig;
                    }
                }

                for (uint64_t z = 0; z < prefices.size(); z++) {

                  std::string filename = datadir + prefices[z];
                  if (prefices[z] != "bespoke") {
                      filename += ("/" + lexrepr);
                  }
                  assertfile(filename);

                  int64_t bbox[4] = {0};
                  env.getbbox(bbox);

                  pattern remainder = stell - dimer;
                  pattern dcs = dimer.convolve(sword);

                  if ((dcs & remainder).nonempty() && (dcs(12, 0) & remainder).nonempty() && (dcs(0, 12) & remainder).nonempty()) { continue; }

                  for (uint64_t j = 0; j < 4; j++) {

                    pattern tlt = dimer.shift(-bbox[0], -bbox[1]);
                    if (j & 1) {
                        if (tlt == tlt[1]) { continue; }
                        tlt = tlt[1];
                    }
                    if (j & 2) {
                        if (lexrepr == "xs4_33") { continue; }
                        tlt = tlt.transpose();
                    }
                    auto it = cgfiles[filename].sdata.find(tlt._string32());
                    if (it != cgfiles[filename].sdata.end()) {

                        uint64_t deep_bailout = initbail;
                        uint64_t tree_bailout = initbail * 200;

                        pattern source(lab, lab->_string32(cgfiles[filename].source), rule);

                        uint64_t trycount = 0;

                        for (uint64_t k = 1; k < it->second.size(); k++) {
                            for (uint64_t l = 0; l < it->second[k].size(); l++) {


                                cgsalvo<int16_t> cs = it->second[k][l];

                                // Determine whether it is worth proceeding:
                                bool trythis = false;
                                pattern xlt = source.shift(-cs.dx, -cs.dy);
                                pattern altstell = remainder + xlt.shift(bbox[0], bbox[1]);
                                double altstelllength = stelllength;
                                uint64_t altbdiff = 0;

                                bool oneblock_improvement = false;

                                if (oneblock) {
                                    int64_t bbox2[4] = {0};
                                    xlt.shift(bbox[0], bbox[1]).getrect(bbox2);
                                    bbox2[0] -= ideal[0];
                                    bbox2[1] -= ideal[1];
                                    altbdiff = (bbox2[0] * bbox2[0]) + (bbox2[1] * bbox2[1]);
                                    oneblock_improvement = (std::sqrt((double) altbdiff) <= std::sqrt((double) bdiff) - 25.0 + 0.1 * initbail);
                                    trythis = (altbdiff == 0) || oneblock_improvement;
                                } else if (lexrepr == "xs4_33") {
                                    if (trycount == deep_bailout) {
                                        std::cout << "Reached bailout for strategy 'deep'" << std::endl;
                                    } else if (trycount == tree_bailout) {
                                        std::cout << "Reached bailout for strategy 'tree'" << std::endl;
                                    } else if (trycount > tree_bailout) { break; }
                                    altstelllength = stlength(altstell + unavoid - avoid, cfier);
                                    trythis = (altstelllength <= stelllength - 25.0) && (trycount < tree_bailout);
                                    trythis = trythis || (trycount < deep_bailout);
                                } else {
                                    trythis = true;
                                }

                                if (trythis) {

                                  pattern slt = source;
                                  for (uint64_t m = 0; m < cs.gliders.size(); m++) {
                                      std::pair<int16_t, char> ng = cs.gliders[m];
                                      int64_t posback = (m + 1) * 128;
                                      if (ng.second == 'E') {
                                          slt += eglider(posback + ng.first, posback);
                                      } else {
                                          slt += oglider(posback + ng.first, posback);
                                      }
                                  }
                                  uint64_t j2 = (cs.transpose ? 2 : 0) + cs.age;
                                  j2 ^= j;
                                  slt = slt.shift(-cs.dx, -cs.dy);
                                  pattern xlt = source.shift(-cs.dx, -cs.dy);
                                  if (j2 & 1) { slt = slt[1]; xlt = xlt[1]; }
                                  if (j2 & 2) { slt = slt.transpose(); xlt = xlt.transpose(); }
                                  pattern sltshift = slt.shift(bbox[0], bbox[1]);
                                  pattern newpat = sltshift + remainder;

                                  if (newpat[512 * (cs.gliders.size() + 1)] == stell) {
                                    if ((sltshift.convolve(sword) & remainder).empty()) {
                                        // std::cout << "Good match!" << std::endl;
                                    } else {
                                        // std::cout << "Inaccessible from infinity" << std::endl;
                                        continue;
                                    }

                                    int64_t posback = (cs.gliders.size() + 2) * 128;
                                    newpat += exsalvo(posback, posback);
                                    pattern altstell = remainder + xlt.shift(bbox[0], bbox[1]);
                                    if (oneblock) {
                                        if (altbdiff == 0) {
                                            std::cout << "\033[32;1mInitial block correctly emplaced\033[0m" << std::endl;
                                            return newpat;
                                        } else if (oneblock_improvement) {
                                            std::cout << "\033[32;1mInitial block moved towards target\033[0m" << std::endl;
                                            return newpat;
                                        }
                                    } else if (lexrepr == "xs4_33") {
                                        pattern newpat2 = newpat;
                                        if ((trycount >= lastbail) && (trycount < deep_bailout)) {
                                            // We now create a really large sword to ensure only the BR-most
                                            // block is moved by the 'deep' strategy:
                                            pattern qlt = sltshift.convolve(bigblock).convolve(bigblock);
                                            qlt += qlt(8, 8).convolve(bigblock).convolve(bigblock);
                                            if ((qlt.convolve(sword) & remainder).empty()) {
                                                newpat2 = precurse(newpat, cfier, 0, 1, 1, 0, ideal);
                                            }
                                        }

                                        if (newpat != newpat2) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'deep'\033[0m" << std::endl;
                                            return newpat2;
                                        } else if ((nbespoke == 0) && (altstelllength <= stelllength - 25.0)) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'tree'\033[0m: ";
                                            std::cout << "length " << stelllength << " --> " << altstelllength << std::endl;
                                            return newpat;
                                        } else if ((nbespoke > 0) && (altstelllength <= stelllength)) {

                                            pattern barrier = avoid.convolve(sword);
                                            pattern altrem = altstell - avoid;
                                            pattern rem = stell - avoid;
                                            rem += rem(0, 6); rem += rem(6, 0);
                                            altrem += altrem(0, 6); altrem += altrem(6, 0);
                                            rem += rem(0, 12); rem += rem(12, 0);
                                            altrem += altrem(0, 12); altrem += altrem(12, 0);
                                            uint64_t pop1 = 0;
                                            uint64_t pop2 = 0;
                                            for (uint64_t zz = 0; zz < 4; zz++) {
                                                pop2 += (barrier & altrem).popcount((1 << 30) + 3);
                                                pop1 += (barrier & rem).popcount((1 << 30) + 3);
                                                barrier = barrier.convolve(sword).convolve(sword);
                                            }
                                            if (pop2 < pop1) {
                                                std::cout << "\033[32;1mSimplification made by strategy 'uncover'\033[0m: ";
                                                std::cout << "abhorrence " << pop1 << " --> " << pop2 << std::endl;
                                                return newpat;
                                            }
                                        }
                                    } else {
                                        if (prefices[z] == "bespoke") {
                                            std::cout << "\033[32;1mSimplification made by strategy 'bespoke'\033[0m: " ;
                                        } else if (totobj == 1) {
                                            std::cout << "\033[32;1mSimplification made by strategy 'reduce'\033[0m: " ;
                                        } else {
                                            std::cout << "\033[32;1mSimplification made by strategy 'split'\033[0m: ";
                                        }
                                        std::cout << "population " << stell.popcount((1 << 30) + 3) << " --> ";
                                        std::cout << altstell.popcount((1 << 30) + 3) << std::endl;
                                        return newpat;
                                    }
                                  }
                                }

                                trycount += 1;
                            }
                        }
                    }
                  }
                }
            }
            // std::cout << "--------------------------------" << std::endl;
            if ((maxbail != 0) && (initbail < maxbail)) {
                std::cout << "Increasing bailout to " << (initbail * 3) << std::endl;
                return precurse(orig, cfier, state, initbail * 3, maxbail, initbail, ideal);
            }
            return orig;
        }

        pattern preiterate(pattern initial, classifier &cfier, int64_t *ideal) {
            pattern pcend = initial;
            pattern pcstart(initial.getlab(), "", initial.getrule());
            while (pcstart != pcend) {
                pcstart = pcend;
                pcend = precurse(pcstart, cfier, 1, 3, 4096, 0, ideal);
                // pcend.write_macrocell(std::cout);
            }
            return pcend;
        }

        pattern preiterate(pattern initial, classifier &cfier) {
            pattern findme(initial.getlab(), "3b2o$2bo2bo$bob2obo$obo2bobo$obo2bobo$bob2obo$2bo2bo$3b2o!", initial.getrule());
            pattern m = initial.match(findme);
            pattern im = initial - m.convolve(findme);

            if (m.empty()) {
                return preiterate(im, cfier, 0);
            } else {
                int64_t ideal[4] = {0};
                m(3, 3).getrect(ideal);
                return preiterate(im, cfier, ideal);
            }
        }

    };

}


