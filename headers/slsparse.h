#pragma once

#include "slmake.h"
#include "sloptim.h"
#include "slperp.h"

namespace apg {

    pattern cell_lowerright(pattern inpat) {
        // Get a reasonably lower-right cell of an input pattern.
        bitworld bw = inpat.flatlayer(0);
        bw = bw.br1cell();
        auto lab = inpat.getlab();
        pattern brcell(lab, lab->demorton(bw, 1), inpat.getrule());
        return brcell;
    }

    struct sparselab {

        cghq cg;
        elbowlab el;
        classifier cfier;
        std::string corderpush;
        std::string normalpush;
        lifetree_abstract<uint32_t>* lt2state;

        sparselab(lifetree_abstract<uint32_t>* lt) : cfier(lt, "b3s23") {

            lt2state = lt;

            std::cerr << "Loading elbow recipes..." << std::endl;

            cg.datadir = "data/";
            el.loadrecs("data/simeks/pp.txt");
            el.load0deg("data/simeks/pp0deg.txt");
            el.corderpush2 = el.loadpush("data/corderpush2.txt");
            el.crabpush = el.loadpush("data/crabpush.txt");

            el.loadsm("data/snarkmaker.txt");
            std::ifstream ins("data/corderpush.txt");
            std::string item;
            while (std::getline(ins, item, '\n')) {
                corderpush += " " + item;
            }
            normalpush = "[0,109,91,93,90,132,115,127,91,90,91,95,90,114,162,233,159,90,155,126,93,118,90,91,90,90] 7move17";

            std::cerr << "...elbow recipes loaded." << std::endl;
        }

        pattern get_exclusions(pattern inpat) {

            auto lab = inpat.getlab();

            pattern excl(lab, "", "b3s23");

            bitworld live = inpat.flatlayer(0);
            std::vector<bitworld> clusters = cfier.getclusters(live, live, true);
            for (uint64_t i = 0; i < clusters.size(); i++) {
                if (clusters[i].population() >= 32) {
                    excl += pattern(lab, lab->demorton(clusters[i], 1), "b3s23");
                }
            }

            return excl;
        }

        uint64_t excluded_popcount(pattern inpat) {
            pattern excl = get_exclusions(inpat);
            return (inpat - excl).popcount((1 << 30) + 3);
        }

        pattern get_lowerright(pattern inpat, uint64_t minpop, int radius) {
            // Get a lower-right chunk of an input pattern.
            auto lab = inpat.getlab();
            pattern convrect(lab, lab->rectangle(-radius, -radius, 2 * radius + 1, 2 * radius + 1), "b3s23");
            pattern rempat = inpat;
            pattern diag = diagonalise(inpat);

            pattern chunk(lab, "", "b3s23");

            while (rempat.nonempty() && (excluded_popcount(chunk) < minpop)) {
                chunk = pattern(lab, "", "b3s23");
                pattern newchunk = chunk + cell_lowerright(rempat);

                while (chunk != newchunk) {
                    int64_t bbox[4] = {0};
                    newchunk.getrect(bbox);
                    chunk = pattern(lab, lab->rectangle(bbox[0], bbox[1], bbox[2], bbox[3]), "b3s23");
                    newchunk = chunk.convolve(convrect);
                    newchunk += newchunk(radius * 8, radius * 8).convolve(convrect);
                    newchunk = newchunk.convolve(diag) & inpat;
                    chunk &= inpat;
                }
                rempat -= chunk;
            }

            rempat = inpat - chunk;
            if (rempat.nonempty() && (excluded_popcount(chunk) < minpop)) {
                std::cout << "Increasing radius from " << radius << " to " << (radius * 2) << std::endl;
                chunk = get_lowerright(inpat, minpop, radius * 2);
            } else {
                std::cout << "\033[36;1mFound " << excluded_popcount(chunk);
                std::cout << "-cell chunk; " << excluded_popcount(rempat);
                std::cout << " cells remain.\033[0m" << std::endl;
            }

            return chunk;
        }

        bool is_narrow(pattern infi, pattern m) {

            pattern inf(lt2state, "", "b3s23"); inf += infi;
            auto lab = inf.getlab();

            pattern findme(lab, "4$2bo$bobob2o$o2bob2o$obo$bo!", "b3s23");
            pattern tri(lab, "5o$4o$3o$2o$o!", "b3s23");
            tri = tri(-2, -2);

            int64_t bbox[4] = {0};
            (inf + m).getrect(bbox);

            int64_t x = bbox[2] + bbox[3];

            while (x > 0) {
                tri = tri.convolve(tri);
                x = x >> 1;
            }

            tri = m.convolve(tri.bror());

            pattern infx = inf - m.convolve(findme).bror();
            return ((infx & tri(45, 45)) - tri(-45, -45) == infx);
        }

        std::pair<pattern, pattern> diagsplit(pattern inf, pattern m, bool duplicate) {

            auto lab = inf.getlab();

            pattern findme(lab, "4$2bo$bobob2o$o2bob2o$obo$bo!", "b3s23");
            pattern tri(lab, "5o$4o$3o$2o$o!", "b3s23");
            tri = tri(-2, -2);

            int64_t bbox[4] = {0};
            (inf + m).getrect(bbox);

            int64_t x = bbox[2] + bbox[3];

            while (x > 0) {
                tri = tri.convolve(tri);
                x = x >> 1;
            }

            tri = m.convolve(tri.bror());

            pattern infx = inf - m.convolve(findme).bror();

            pattern middle(lt2state, "", "b3s23");
            middle += ((infx & tri(30, 30)) - tri(-30, -30));

            if (middle.nonempty()) {
                if (duplicate) {
                    return std::pair<pattern, pattern>(infx, infx);
                } else {
                    return std::pair<pattern, pattern>(infx, pattern(lab, "", "b3s23"));
                }
            }

            pattern infy = infx & tri;
            pattern infz = infx - tri;
            return std::pair<pattern, pattern>(infy, infz);
        }

        cgsalvo<int64_t> sparsebuild(pattern infx, int64_t* bbox, int64_t* bbox3, bool &existing_hand) {
            /*
            * More scalable version of cg.preiterate.
            */

            // Coerce to a 2-state pattern:
            pattern inf(lt2state, "", "b3s23"); inf += infx;

            std::cout << "\033[36;1mCalling sparsebuild on initial pattern comprising ";
            std::cout << (inf.popcount((1 << 30) + 3)) << " cells\033[0m" << std::endl;

            cgsalvo<int64_t> cs;
            do {
                // Capture any existing gliders:
                pattern spat = inf & inf[2] & inf[4] & inf[6] & inf[8];
                cgsalvo<int64_t> cs2;
                cs2.glidermatch(inf - spat);
                for (uint64_t i = 0; i < cs.gliders.size(); i++) {
                    cs2.gliders.push_back(cs.gliders[i]);
                }
                cs = cs2;

                if (excluded_popcount(spat) == 0) { existing_hand = true; break; }

                // Get next cluster and construct it:
                pattern brcluster = get_lowerright(spat, 24, 40);
                pattern leftover = spat - brcluster;
                if (leftover.empty()) {
                    (brcluster - get_exclusions(brcluster)).getrect(bbox);
                    if ((bbox[2] == 2) && (bbox[3] == 2)) {
                        if ((bbox[0] != bbox3[0]) || (bbox[1] != bbox3[1])) {
                            std::cout << "\033[36;1mEmplacing initial block...\033[0m" << std::endl;

                            if (existing_hand) {
                                inf = cg.preiterate(brcluster, cfier, bbox3);
                            } else {
                                if ((bbox[0] + bbox[1] + bbox3[0] + bbox3[1]) % 2) {
                                    bbox[0] -= 1;
                                }
                                int64_t diag = ((bbox3[0] + bbox3[1]) - (bbox[0] + bbox[1])) / 2;
                                if (diag < 800) {
                                    bbox[0] += diag;
                                    bbox[1] += diag;
                                } else {
                                    bbox[0] += (diag % 8);
                                    bbox[1] += (diag % 8);
                                }
                                inf = cg.preiterate(brcluster, cfier, bbox);
                            }
                        } else {
                            inf = brcluster;
                        }
                    } else {
                        inf = cg.preiterate(brcluster, cfier, 0);
                    }
                } else {
                    inf = leftover + cg.preiterate(brcluster, cfier, 0);
                }
            } while (excluded_popcount(inf) != 4);
            std::cout << "\033[36;1mConstruction complete; defragmenting...\033[0m" << std::endl;
            inf -= get_exclusions(inf);
            inf.getrect(bbox); inf = inf(-bbox[0], -bbox[1]);
            for (uint64_t i = 0; i < cs.gliders.size(); i++) {
                cs.gliders[i].first += (bbox[1] - bbox[0]);
            }
            cs = defragment(inf, cs, 10);

            return cs;
        }


        bool find_hands(pattern &infx, int64_t* bbox3) {

            auto lab = infx.getlab();
            pattern findme2(lab, "3b2o$2bo2bo$bob2obo$obo2bobo$obo2bobo$bob2obo$2bo2bo$3b2o!", "b3s23");
            pattern findme3(lab, "3$3b2H$3b2H!", "b3s23");
            pattern replaceme2(lab, "3$3b2o$3b2o!", "b3s23");

            pattern existing_hands = infx.match(findme2);
            infx -= existing_hands.convolve(findme2);
            existing_hands += infx.match(findme3);

            bool existing_hand = existing_hands.nonempty();

            if (existing_hand) {
                existing_hands.convolve(replaceme2).getrect(bbox3);
            }

            return existing_hand;
        }

        bool ne_constell(pattern infx, int64_t* bbox2, scstream &scs) {

            std::cerr << "Trying ne_constell()" << std::endl;

            auto lab = infx.getlab();

            pattern origin_block(lab, "oo$oo!", "b3s23");
            origin_block = origin_block(bbox2[0], bbox2[1]);

            // Now we reflect:
            pattern to_build = infx.transform("flip_x", 0, 0);
            origin_block = origin_block.transform("flip_x", 0, 0);

            scstream elbow_dup("[0, 109, 91, 93, 90, 171, 90, 90, 91, 154, 110, 169, 107, 91, 90, 99, 91, 122, 90, 90, 159, 90, 90,109,90,93,91,91,90,90,100,90, 90,146,96,90,90,90,92,156,144,90,109, 91,93,91,132,115,102,90,91,91,91,90,90,154,98] 0move-52");

            int64_t bbox[4] = {0ull};
            to_build.getrect(bbox);
            int64_t bbox3[4] = {0ull};
            origin_block.getrect(bbox3);

            int64_t forwx = bbox3[0] - bbox[0] - bbox[2];
            int64_t forwy = bbox3[1] - bbox[1] - bbox[3];

            // Number of full-diagonals to move origin block forwards.
            int64_t fd_forward = (forwx < forwy) ? forwx : forwy;

            origin_block = origin_block(-fd_forward, -fd_forward);
            origin_block.getrect(bbox3);

            std::vector<lanespec> paralanes;

            std::string ba = "22$172bo$171bobo$170bobo$169bobo$168bobo$167bobo$166bobo$165bobo$";
            ba += "164bobo$163bobo$162bobo$161bobo$160bobo$159bobo$158bobo$157bobo$156bob";
            ba += "o$155bobo$154bobo$153bobo$154bo107$44bo$43bobo$42bobo$41bobo$40bobo$";
            ba += "39bobo$38bobo$37bobo$36bobo$35bobo$34bobo$33bobo$32bobo$31bobo$30bobo$";
            ba += "29bobo$28bobo$27bobo$26bobo$25bobo$26bo!";

            pattern blocking_apparatus(lab, ba, "b3s23");

            to_build += blocking_apparatus(bbox3[0], bbox3[1]);

            bool existing_hand = find_hands(to_build, bbox3);
            bool always_true = true;

            cgsalvo<int64_t> cs = sparsebuild(to_build, bbox, bbox3, always_true);

            for (auto ng = cs.gliders.begin(); ng != cs.gliders.end(); ++ng) {
                int64_t il = ng->first - 3;
                paralanes.emplace_back(il, std::pair<char, char>('p', ng->second));

                if ((il < -107) || (il > 107)) { return false; }

            }

            int64_t middle = 2 * fd_forward - 160;

            // Now actually operate the elbow to emit the gliders:

            el.desirate(scs, 2 * fd_forward);
            if (!existing_hand) { scs.concat(elbow_dup, 0); }

            for (auto it = paralanes.begin(); it != paralanes.end(); ++it) {
                if (!el.fire_parallel(scs, *it, middle)) { std::cerr << "Fatal error!" << std::endl; }
            }

            return true;
        }

        void nw_constell(pattern infx, int64_t* bbox2, scstream &scs, bool new_elbow) {

            int64_t bbox3[4] = {bbox2[0] - 47, bbox2[1] - 42, bbox2[2], bbox2[3]};

            pattern to_build = infx; bool existing_hand = find_hands(to_build, bbox3);

            int64_t bbox[4] = {0}; cgsalvo<int64_t> cs = sparsebuild(to_build, bbox, bbox3, existing_hand);

            int64_t xm3 = bbox[0] - bbox3[0];
            int64_t ym3 = bbox[1] - bbox3[1];
            std::cout << "(" << xm3 << ", " << ym3 << ")" << std::endl;
            int64_t forwdiag = xm3 - ym3;
            int64_t outdiag = -(xm3 + ym3) / 16;

            if (new_elbow) {
                int64_t moveback = (scs.endblocks.back() - forwdiag) / 2;
                if (moveback >= 1000) { scs.open_paren(moveback); }
            }

            el.desirate(scs, forwdiag);

            if (!existing_hand) {

                scstream scs2((outdiag >= 21) ? corderpush : normalpush);

                for (uint64_t i = 0; i < scs2.gstream.size(); i++) {
                    if (scs2.gstream[i] == 195) {
                        scs2.gstream[i] += ((outdiag - 21) * 64);
                        std::cout << "Delta increased to " << scs2.gstream[i] << std::endl;
                    }
                }

                scs.concat(scs2, 0);

            }

            int64_t xmove = bbox[0] - bbox2[0];
            int64_t ymove = bbox[1] - bbox2[1];

            for (uint64_t m = 0; m < cs.gliders.size(); m++) {
                std::pair<int64_t, char> ng = cs.gliders[m];
                int64_t il = 4 + xmove - ymove + ng.first;
                el.desirate(scs, lanespec(il, std::pair<char, char>('i', ng.second)));
            }
            // el.desirate(scs, 0);

            // return scs;
        }

        pattern target_to_glider(pattern inf) {

            auto lab = inf.getlab();
            pattern resultant(lab, "", "b3s23");
            pattern findme(lab, "6$4bo$3bobob2o$2bo2bob2o$2bobo$3bo!", "b3s23");
            pattern replaceme(lab, "7$7b2o$7b2o2$3o$2bo$bo!", "b3s23");
            pattern findme2(lab, "3b2o$2bo2bo$bob2obo$obo2bobo$obo2bobo$bob2obo$2bo2bo$3b2o!", "b3s23");
            pattern replaceme2(lab, "3$3b2o$3b2o!", "b3s23");
            pattern findme3(lab, "3$3b2H$3b2H!", "b3s23");

            for (int orient = 0; orient < 8; orient++) {

                std::string forward;
                std::string reverse;

                switch (orient) {
                    case 0: forward = "identity"; reverse = "identity"; break;
                    case 1: forward = "rot180"; reverse = "rot180"; break;
                    case 2: forward = "rot90"; reverse = "rot270"; break;
                    case 3: forward = "rot270"; reverse = "rot90"; break;
                    case 4: forward = "flip_x"; reverse = "flip_x"; break;
                    case 5: forward = "flip_y"; reverse = "flip_y"; break;
                    case 6: forward = "swap_xy"; reverse = "swap_xy"; break;
                    case 7: forward = "swap_xy_flip"; reverse = "swap_xy_flip"; break;
                }

                pattern inft = inf(forward, 0, 0);

                pattern m = inft.match(findme);
                if (m.empty()) { continue; }

                pattern infx = m.convolve(replaceme);
                infx += inft.match(findme2).convolve(replaceme2);
                infx += inft.match(findme3).convolve(replaceme2);

                resultant = infx(reverse, 0, 0);

            }

            return resultant;

        }

        void resolve_metacluster(pattern infx, pattern m, scstream &scs, bool new_elbow) {

            auto lab = infx.getlab();
            pattern findme(lab, "4$2bo$bobob2o$o2bob2o$obo$bo!", "b3s23");
            pattern findme2(lab, "$2b2o$2b2o2$2b2o$bo2bo$o2bo$b2o!", "b3s23");

            int64_t ideal[4] = {0};
            m(5, 5).getrect(ideal);

            pattern clr = cell_lowerright(infx);
            int64_t bbox[4] = {0};
            clr.getrect(bbox);
            bool wrong_orientation = (bbox[0] + bbox[1] > ideal[0] + ideal[1]);
            infx.getrect(bbox);

            auto things = diagsplit(infx, m, true);
            int64_t span_both_sides = (bbox[0] + bbox[1] + bbox[2] + bbox[3]) - (ideal[0] + ideal[1]);

            if ((things.first.nonempty()) && (things.second.nonempty())) {
                std::cerr << "Metacluster spans both sides of the channel." << std::endl;
                while (scs.endblocks.size() > 1) { scs.close_paren(); }

                if (is_narrow(infx, m) && ne_constell(infx, ideal, scs)) { return; }

                int64_t forwdiag = (bbox[0] - ideal[0]) - (bbox[1] + bbox[3] - ideal[1]) - 128;
                forwdiag = 2 * (forwdiag / 2) + 1; // Ensure correct parity
                el.desirate(scs, forwdiag);

                int64_t fdshift = (span_both_sides / 2) + 128;

                std::cerr << "Fdshift: " << fdshift << std::endl;

                ideal[0] += 2 * fdshift;

                apg::scstream smaker = el.make_snark(fdshift);
                apg::scstream sbreaker = el.break_snark(fdshift);

                std::cerr << "Snarkmaker length: " << smaker.gstream.size() << std::endl;
                std::cerr << "Snarkbreaker length: " << sbreaker.gstream.size() << std::endl;

                scs.concat(smaker, 0);
                scs.concat(smaker, 0);

                nw_constell(infx, ideal, scs, false);

                el.desirate(scs, forwdiag);
                scs.concat(sbreaker, 0);
                scs.concat(sbreaker, 0);

            } else if (wrong_orientation) {
                std::cerr << "Wrong orientation; flipping pattern." << std::endl;
                pattern inft = infx + m.convolve(findme2);
                inft = inft("swap_xy_flip", 0, 0);

                pattern m2 = inft.match(findme);
                if (m2.empty()) { std::cerr << "This should never happen!" << std::endl; }
                pattern infx2 = inft - m2.convolve(findme);
                m2(5, 5).getrect(ideal);

                scs.translate(-1);
                nw_constell(infx2, ideal, scs, new_elbow);
                scs.translate(1);

            } else {
                std::cerr << "Correct orientation; proceeding as normal." << std::endl;
                nw_constell(infx, ideal, scs, new_elbow);
            }

        }


        void into_clusters(pattern &infx, std::map<std::pair<int64_t, int64_t>, std::vector<pattern> > &metaclusters) {

            while (infx.nonempty()) {
                pattern metacluster = cell_lowerright(infx);
                pattern mc_old(infx.getlab(), "", "b3s23");

                int64_t bbox[4] = {0};
                while (metacluster != mc_old) {
                    metacluster.getrect(bbox);
                    mc_old = metacluster;
                    bbox[0] -= 800; bbox[1] -= 800; bbox[2] += 1600; bbox[3] += 1600;
                    metacluster = infx.subrect(bbox);
                }

                int64_t left  = bbox[1] - (bbox[0] + bbox[2]);
                int64_t right = (bbox[1] + bbox[3]) - bbox[0];

                metaclusters[std::pair<int64_t, int64_t>(left, right)].push_back(metacluster);
                infx -= metacluster;
            }

        }

        bool try_orientation(pattern inf, scstream &scs) {

            pattern findme(inf.getlab(), "4$2bo$bobob2o$o2bob2o$obo$bo!", "b3s23");

            pattern m = inf.match(findme);
            if (m.empty()) { return false; }

            int64_t desired_position = scs.endblocks.back();

            std::map<std::pair<int64_t, int64_t>, std::vector<pattern> > premetaclusters;
            into_clusters(inf, premetaclusters);

            std::map<std::pair<int64_t, int64_t>, std::vector<pattern> > metaclusters;
            for (auto it = premetaclusters.begin(); it != premetaclusters.end(); ++it) {
                for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
                    auto hp = diagsplit((*it2), m, false);
                    into_clusters(hp.first, metaclusters);
                    into_clusters(hp.second, metaclusters);
                }
            }

            std::vector<std::vector<pattern> > vvmc(1);
            int64_t lastright = 0;

            for (auto it = metaclusters.begin(); it != metaclusters.end(); ++it) {
                if (vvmc.back().size() > 0) {
                    if (lastright + 1600 < it->first.first) {
                        vvmc.emplace_back();
                    }
                    lastright = (it->first.second > lastright) ? it->first.second : lastright;
                } else {
                    lastright = it->first.second;
                }

                vvmc.back().insert(vvmc.back().end(), it->second.begin(), it->second.end());
            }

            for (auto it = vvmc.begin(); it != vvmc.end(); ++it) {
                std::map<int64_t, std::vector<pattern> > metaclusters2;
                for (auto it2 = it->begin(); it2 != it->end(); ++it2) {
                    int64_t bbox[4] = {0};
                    int64_t bbox2[4] = {0};
                    it2->getrect(bbox);
                    m.getrect(bbox2);

                    int64_t abscissa = ((2*bbox[0]) + bbox[2]) - ((2*bbox2[0]) + bbox2[2]);
                    int64_t ordinate = ((2*bbox[1]) + bbox[3]) - ((2*bbox2[1]) + bbox2[3]);

                    int64_t distance = abscissa + ordinate;
                    if (distance > 0) { distance = 0 - distance; }

                    metaclusters2[distance].push_back(*it2);
                }

                it->clear();

                for (auto it2 = metaclusters2.begin(); it2 != metaclusters2.end(); ++it2) {
                    it->insert(it->end(), it2->second.begin(), it2->second.end());
                }
            }

            std::cout << "Signature: ";

            for (auto it = vvmc.begin(); it != vvmc.end(); ++it) {
                std::cout << "[ ";
                for (auto it2 = it->begin(); it2 != it->end(); ++it2) {
                    std::cout << it2->popcount((1 << 30) + 3) << " ";
                }
                std::cout << "] ";
            }

            std::cout << std::endl;

            // Construct metaclusters:
            for (auto it = vvmc.begin(); it != vvmc.end(); ++it) {
                bool new_elbow = true;
                for (auto it2 = it->begin(); it2 != it->end(); ++it2) {
                    resolve_metacluster(*it2, m, scs, new_elbow);
                    new_elbow = false;
                    std::ofstream f("progress.txt");
                    scs.exportgliders(f);
                }
            }

            // Return block to original position:
            while (scs.endblocks.size() > 1) { scs.close_paren(); }
            el.desirate(scs, desired_position - 8); // cause final move to be 0move8
            el.desirate(scs, desired_position);

            return true;
        }

        bool arbitrary_build(pattern inf, scstream &scs) {

            for (int orient = 0; orient < 8; orient++) {

                std::string forward;
                std::string reverse;

                switch (orient) {
                    case 0: forward = "identity"; reverse = "identity"; break;
                    case 1: forward = "rot180"; reverse = "rot180"; break;
                    case 2: forward = "rot90"; reverse = "rot270"; break;
                    case 3: forward = "rot270"; reverse = "rot90"; break;
                    case 4: forward = "flip_x"; reverse = "flip_x"; break;
                    case 5: forward = "flip_y"; reverse = "flip_y"; break;
                    case 6: forward = "swap_xy"; reverse = "swap_xy"; break;
                    case 7: forward = "swap_xy_flip"; reverse = "swap_xy_flip"; break;
                }

                pattern inft = inf(forward, 0, 0);

                if (try_orientation(inft, scs)) { return true; }
            }

            return false;

        }

        scstream condense(pattern infx, int64_t* bbox2) {
            pattern inf = cg.preiterate(infx, cfier);
            int64_t bbox[4] = {0}; inf.getrect(bbox); inf = inf(-bbox[0], -bbox[1]);
            pattern spat = inf & inf[2] & inf[4] & inf[6] & inf[8];

            cgsalvo<int64_t> cs;
            cs.glidermatch(inf - spat);
            cs = defragment(spat, cs, 10);

            int64_t xmove = bbox[0] - bbox2[0];
            int64_t ymove = bbox[1] - bbox2[1];

            scstream scs;

            for (uint64_t m = 0; m < cs.gliders.size(); m++) {
                std::pair<int64_t, char> ng = cs.gliders[m];
                int64_t il = 4 + xmove - ymove + ng.first;
                el.desirate(scs, lanespec(il, std::pair<char, char>('x', ng.second)));
            }
            el.desirate(scs, 0);

            return scs;
        }

    };

}
