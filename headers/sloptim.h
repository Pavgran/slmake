#pragma once

#include "slmake.h"
#include <cstdlib>
#include <unordered_set>

namespace apg {

    std::pair<bitworld, bitworld> bwdiff(bitworld &a, bitworld &b) {

        bitworld i = a; i &= b;
        bitworld c = a; c -= i;
        bitworld d = b; d -= i;
        return std::pair<bitworld, bitworld>(grow_cluster(c, a, "9"), grow_cluster(d, b, "9"));
    }

    typedef std::pair<std::string, cgsalvo<int64_t> > pcgpair;

    pcgpair collenum(pcgpair inpcg, pattern musthave, pattern finish, int timelimit, std::unordered_set<uint64_t>* seenhashes,
                        std::vector<pcgpair>* allpairs, std::map<std::string, int>* wanted, int threshold, bool lastlayer) {
        /*
        * Enumerate slow-salvo glider collisions with a pattern.
        */

        lifetree_abstract<uint32_t> *lab = musthave.getlab();
        pattern target(lab, lab->_string32(inpcg.first), musthave.getrule());
        pattern envelope = target + target[1];
        int64_t bbox[4] = {0};
        envelope.getrect(bbox);

        // target.display(bbox);
        // std::cout << "-------------------------------------" << std::endl;

        int emaxx = bbox[0] + bbox[2] + 4;
        int emaxy = bbox[1] + bbox[3] + 4;
        pattern diagonal = diagonalise(envelope);
        int popcount = diagonal.popcount((1 << 30) + 3);
        diagonal += diagonal(-popcount, -popcount);
        int depth = envelope.gethnode().depth;

        envelope -= (musthave + musthave[1]);
        envelope.convolve(diagonal).subrect(-(16 << depth), bbox[1], 32 << depth, 1).getrect(bbox);
        int starti = bbox[0] - bbox[1] - 5;
        int endi = starti + bbox[2] + 9;

        for (uint8_t j = 0; j < 2; j++) {
            std::string pstring = ((j == 0) ? "3o$o$bo!" : "boo$oo$bbo!");
            pattern perturber(target.getlab(), pstring, target.getrule());
            // Iterate over different glider lanes:
            for (int i = starti; i < endi; i++) {
                int tx = (i >= (emaxx - emaxy)) ? emaxx : i + emaxy;
                int ty = (i <= (emaxx - emaxy)) ? emaxy : emaxx - i;
                pattern seed = target + perturber.shift(tx, ty);
                pattern finseed = seed[timelimit];
                if ((!(finseed.empty())) && (musthave - finseed).empty() && (finseed == finseed[2])) {
                    uint64_t dig = finseed.digest();
                    if ((seenhashes == 0) || (seenhashes->count(dig) == 0)) {
                        if (seenhashes != 0) { seenhashes->insert(dig); }
                        std::string fss = finseed._string32();
                        bool success = false;

                        // This function can be called in one of two ways,
                        // depending on whether we're matching against one
                        // target (for the reverse tree) or against multiple
                        // targets (for the forward tree). We indicate the
                        // former by providing a null pointer for 'wanted':
                        if (wanted != 0) {
                            auto it = wanted->find(fss);
                            success = ((it != wanted->end()) && (it->second >= threshold));
                        } else {
                            success = (finish == finseed);
                        }

                        if (success) {
                            pcgpair outpcg(fss, inpcg.second);
                            outpcg.second.gliders.emplace_back(i, "EO"[j]);
                            return outpcg;
                        }

                        // Record the output for future collisions:
                        if ((allpairs != 0) && ((!lastlayer) || ((finseed - target).empty() && (target - finseed - finish).empty()))) {
                            allpairs->emplace_back(fss, inpcg.second);
                            allpairs->back().second.gliders.emplace_back(i, "EO"[j]);
                        }
                    }
                }
            }
        }

        return inpcg;
    }

    std::vector<pcgpair> collobj(pattern inpat, int timelimit) {
        /*
        * Return results of all single-glider collisions with a particular pattern.
        */

        std::vector<pcgpair> allpairs;
        pattern musthave = inpat - inpat;
        std::unordered_set<uint64_t> seenhashes;
        cgsalvo<int64_t> cgempty;
        cgempty.dx = 0; cgempty.dy = 0; cgempty.age = 0; cgempty.transpose = 0;
        pcgpair inpcg(inpat._string32(), cgempty);
        collenum(inpcg, musthave, musthave, timelimit, &seenhashes, &allpairs, 0, 0, 0);
        return allpairs;

    }

    void encoll(pattern inpat, int timelimit, std::map<std::string, std::set<std::string> >* sssmap) {

        std::vector<pcgpair> pcgv = collobj(inpat, timelimit);
        // std::cout << pcgv.size() << " distinct results of collision" << std::endl;
        for (uint64_t i = 0; i < pcgv.size(); i++) {
            pattern q(inpat.getlab(), inpat.getlab()->_string32(pcgv[i].first), inpat.getrule());
            int64_t bbox[4] = {0};
            q.getrect(bbox);
            pattern p = inpat(-bbox[0], -bbox[1]);
            q = q(-bbox[0], -bbox[1]);
            if ((bbox[2] <= 16) && (bbox[3] <= 16)) {
                (*sssmap)[q._string32()].insert(p._string32());
            }
        }

    }

    bool colltest(pattern start, pattern finish, pattern musthave, int timelimit) {
        /*
        * Determine whether a single glider can transform 'start' into
        * 'finish' within 'timelimit' generations.
        */

        if (start != start[2]) { return false; }
        cgsalvo<int64_t> cgempty;
        cgempty.dx = 0; cgempty.dy = 0; cgempty.age = 0; cgempty.transpose = 0;
        pcgpair inpcg(start._string32(), cgempty);
        pcgpair outpcg = collenum(inpcg, musthave, finish, timelimit, 0, 0, 0, 0, 0);
        return outpcg.second.gliders.size();

    }

    void decoll(pattern src, pattern inpat, int timelimit, std::set<std::string>* output,
                classifier &cfier) {

        pattern inter = src & inpat;
        bitworld srcbw = src.flatlayer(0);
        bitworld inter2 = grow_cluster(inter.flatlayer(0), srcbw, "9");
        srcbw -= inter2;
        std::vector<bitworld> clusters = cfier.getclusters(srcbw, srcbw, false);

        for (uint64_t i = 0; i < clusters.size(); i++) {
            pattern cpat(src.getlab(), src.getlab()->demorton(clusters[i], 1), src.getrule());
            pattern outpat = inpat + cpat;
                        std::string oh = outpat._string32();
                        if (output->count(oh) == 0) {
                            if (colltest(outpat, inpat, inpat, timelimit)) {
                                output->insert(oh);
                            }
                        }
        }
    }

    void precoll(pattern inpat, pattern musthave, int timelimit,
                    std::map<std::string, std::set<std::string> >* sssmap,
                    std::set<std::string>* output) {
        /*
        * Like collobj, but find predecessors instead of successors.
        */
        pattern rem = inpat - musthave;
        for (auto it = sssmap->begin(); it != sssmap->end(); ++it) {
            pattern q(inpat.getlab(), inpat.getlab()->_string32(it->first), inpat.getrule());
            pattern r = q + q(1, 0); r += r(-1, 0); r += r(0, 1); r += r(0, -1); r -= q;
            pattern m = rem.match(q, r);
            if (!m.empty()) {
                bitworld bw = m.flatlayer(0);
                int64_t bbox[4] = {0};
                while (bw.population()) {
                    bitworld onecell = bw.get1cell();
                    bw -= onecell;
                    onecell.getbbox(bbox);
                    pattern z = inpat - q(bbox[0], bbox[1]);
                    // std::cout << it->second.size() << " candidates." << std::endl;
                    for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
                        pattern p(inpat.getlab(), inpat.getlab()->_string32(*it2), inpat.getrule());
                        pattern outpat = z + p(bbox[0], bbox[1]);
                        std::string oh = outpat._string32();
                        if (output->count(oh) == 0) {
                            if (colltest(outpat, inpat, z, timelimit)) {
                                output->insert(oh);
                            }
                        }
                    }
                }
            }
        }
    }

    void tellmewhatyouwant(pattern src, pattern dest, pattern musthave, int timelimit, int distance, int maxback,
                    std::map<std::string, std::set<std::string> >* sssmap, std::map<std::string, int>* wanted,
                    classifier &cfier) {

        int mb = maxback;
        if (distance < 2) { return; }
        if (mb > distance - 2) { mb = distance - 2; }
        // std::cout << "Recursing back by " << mb << " steps..." << std::endl;

        std::vector<std::set<std::string> > colls(maxback + 1);
        colls[0].insert(dest._string32());

        for (int i = 0; i <= mb; i++) {
            for (auto it = colls[i].begin(); it != colls[i].end(); it++) {
                int p = (*wanted)[*it];
                int q = distance - i - 1;
                if (q > p) {
                    (*wanted)[*it] = q;
                    if (i < mb) {
                        pattern inpat(musthave.getlab(), musthave.getlab()->_string32(*it), musthave.getrule());
                        precoll(inpat, musthave, timelimit, sssmap, &(colls[i+1]));
                        if (maxback >= 4) { decoll(src, inpat, timelimit, &(colls[i+1]), cfier); }
                    }
                }
            }
        }
    }

    bool collayer(pattern initial, uint64_t &index, pattern musthave, int timelimit, std::unordered_set<uint64_t>* seenhashes,
                        std::vector<pcgpair>* allpairs, std::map<std::string, int>* wanted, bool lastlayer,
                        int threshold, uint64_t mmindex) {

        uint64_t maxindex = allpairs->size();
        if (maxindex > mmindex) { maxindex = mmindex; }
        while (index < maxindex) {
            int threshold2 = threshold;
            pcgpair inpcg = allpairs->at(index);
            int newsize = inpcg.second.gliders.size();
            int origsize = newsize;
            int oldsize = -1;
            while (oldsize != newsize) {
                inpcg = collenum(inpcg, musthave, initial, timelimit, seenhashes,
                                    allpairs, wanted, threshold2, lastlayer);
                oldsize = newsize;
                newsize = inpcg.second.gliders.size();
                auto it = wanted->find(inpcg.first);
                if (it != wanted->end()) { threshold2 = it->second + 1; }
            }
            if (newsize != origsize) {
                allpairs->push_back(inpcg);
                return true;
            }
            index += 1;
        }

        return false;
    }

    pcgpair colltree(pattern initial, pattern musthave, int timelimit, std::map<std::string, int>* wanted, int maxdepth) {

        cgsalvo<int64_t> cgempty;
        cgempty.dx = 0; cgempty.dy = 0; cgempty.age = 0; cgempty.transpose = 0;
        std::unordered_set<uint64_t> seenhashes;
        seenhashes.insert(initial.digest());
        std::vector<pcgpair> allpairs;
        pcgpair nullpcg(initial._string32(), cgempty);
        allpairs.push_back(nullpcg);
        uint64_t index = 0;

        // Stop things from being atypically explosive:
        uint64_t mmindex = 4 << (3 * maxdepth);

        for (int i = 1; i <= maxdepth + 2; i++) {
            if (collayer(initial, index, musthave, timelimit, &seenhashes, &allpairs, wanted, (i >= maxdepth), i+1, mmindex)) {
                std::cout << "True!" << std::endl;
                return allpairs.back();
            }
            if (index == allpairs.size()) { break; }
            std::cout << allpairs.size() << " things produced after " << i << " iterations." << std::endl;
        }

        return nullpcg;

    }

    pcgpair collmitm(std::vector<pattern> &pats, uint64_t bindex, int timelimit, int maxforward, int maxback,
                    std::map<std::string, std::set<std::string> >* sssmap, classifier &cfier,
                    std::map<std::pair<std::string, std::string>, int>* remwant) {

        pattern initial = pats[bindex];
        pattern musthave = initial;
        uint64_t mindex = bindex + 20;

        // int64_t bbox[4] = {0};
        // initial.getrect(bbox); initial.display(bbox);

        if (mindex > pats.size()) { mindex = pats.size(); }
        for (uint64_t i = bindex + 1; i < mindex; i++) {
            musthave &= pats[i];
            musthave &= musthave[2];
        }

        std::cout << "Recursing backwards..." << std::endl;
        std::map<std::string, int> wanted;
        for (uint64_t i = bindex + 2; i < mindex; i++) {
            pattern result = pats[i];
            std::pair<std::string, std::string> pcode(initial._string32(), result._string32());
            if ((remwant == 0) || ((*remwant)[pcode] < maxback)) {
                tellmewhatyouwant(initial, result, musthave, timelimit, i - bindex, maxback, sssmap, &wanted, cfier);
                if (remwant != 0) { (*remwant)[pcode] = maxback; }
            }
        }

        // if (!musthave.empty()) { musthave.getrect(bbox); musthave.display(bbox); }
        pcgpair pcg = colltree(initial, musthave, timelimit, &wanted, maxforward);
        return pcg;

    }

    std::vector<pattern> regenerate(pattern initial, cgsalvo<int64_t> &cg) {

        std::vector<pattern> h;
        h.push_back(initial);
        pattern eglider(initial.getlab(), "3o$o$bo!", initial.getrule());
        pattern oglider(initial.getlab(), "b2o$2o$2bo!", initial.getrule());

        for (uint64_t x = 0; x < cg.gliders.size(); x++) {
            pattern target = h.back();
            pattern envelope = target + target[1];
            int64_t bbox[4] = {0};
            envelope.getrect(bbox);
            int emaxx = bbox[0] + bbox[2] + 4;
            int emaxy = bbox[1] + bbox[3] + 4;
            int64_t i = cg.gliders[x].first;
            pattern perturber = (cg.gliders[x].second == 'O') ? oglider : eglider;
            int tx = (i >= (emaxx - emaxy)) ? emaxx : i + emaxy;
            int ty = (i <= (emaxx - emaxy)) ? emaxy : emaxx - i;
            pattern seed = target + perturber.shift(tx, ty);

            uint64_t sg = 2048 + (((bbox[2] + bbox[3]) >> 6) << 8);
            seed = seed[sg];
            if (seed != seed[2]) { break; }
            h.push_back(seed);
        }

        return h;
    }

    bool testreplace(std::vector<pattern> &pvec, cgsalvo<int64_t> &cg, uint64_t b, cgsalvo<int64_t> &r) {
        /*
        * Replace a sequence of gliders with another equal-length sequence
        * provided they have the same effect. This takes time O(n), where
        * n is the length of the replaced sequence, and does not depend on
        * the overall length of the salvo.
        */

        std::vector<pattern> nvec = regenerate(pvec[b], r);
        uint64_t l = r.gliders.size();

        bool success = (nvec.size() == (l + 1)) && (pvec[b + l] == nvec[l]);

        if (success) {
            for (uint64_t i = 0; i < l; i++) {
                pvec[b + i] = nvec[i];
                cg.gliders[b + i] = r.gliders[i];
            }
        }

        return success;
    }

    int64_t extreplace(std::vector<pattern> &pvec, cgsalvo<int64_t> &cg, uint64_t x, int64_t h2) {

        if (x == 0) { return 0; }
        if (x >= pvec.size() - 1) { return 0; }

        int64_t m2 = cg.gliders[x-1].first + cg.gliders[x].first;
        int64_t leftsgn = (cg.gliders[x-1].first > cg.gliders[x].first) ? 1 : -1;
        int64_t gapwidth = (cg.gliders[x-1].first - cg.gliders[x].first) * leftsgn;

        if (gapwidth < h2) { return 0; }

        uint64_t y = x - 1;
        while (true) {
            if ((y == 0) || ((cg.gliders[y-1].first * 2 - m2) * leftsgn < h2)) { break; }
            y -= 1;
        }
        uint64_t z = x + 1;
        while (true) {
            if ((z == pvec.size() - 1) || ((m2 - cg.gliders[z].first * 2) * leftsgn < h2)) { break; }
            z += 1;
        }

        apg::cgsalvo<int64_t> r;
        for (uint64_t i = x; i < z; i++) {
            r.gliders.push_back(cg.gliders[i]);
        }
        for (uint64_t i = y; i < x; i++) {
            r.gliders.push_back(cg.gliders[i]);
        }

        int64_t saving = std::llabs(cg.gliders[x].first - cg.gliders[x-1].first);
        saving -= std::llabs(cg.gliders[y].first - cg.gliders[z-1].first);

        if (y > 0) {
            saving += std::llabs(cg.gliders[y].first - cg.gliders[y-1].first);
            saving -= std::llabs(cg.gliders[x].first - cg.gliders[y-1].first);
        }
        if (z < cg.gliders.size()) {
            saving += std::llabs(cg.gliders[z].first - cg.gliders[z-1].first);
            saving -= std::llabs(cg.gliders[z].first - cg.gliders[x-1].first);
        }

        if (saving <= 0) { return 0; }

        // std::cout << y << " " << x << " " << z << " : " << saving << std::endl;

        if (testreplace(pvec, cg, y, r)) {
            return saving;
        } else {
            return 0;
        }
    }

    cgsalvo<int64_t> defragment(pattern initial, cgsalvo<int64_t> &cginit, int64_t h2) {

        cgsalvo<int64_t> cg = cginit;
        std::vector<pattern> pvec = regenerate(initial, cg);

        if (pvec.size() < 2) {
            std::cout << "Salvo too short to warrant defragmentation." << std::endl;
            return cg;
        }

        int64_t saving = 1;
        int64_t totsaving = 0;

        while (saving != 0) {
            saving = 0;
            // Ascending pass:
            for (uint64_t x = 1; x <= pvec.size() - 2; x++) {
                int64_t k = extreplace(pvec, cg, x, h2);
                saving += k;
                if (k > 0) { std::cout << "L1 norm reduced by " << k << std::endl; }
            }
            // Descending pass:
            for (uint64_t x = pvec.size() - 2; x >= 1; x--) {
                int64_t k = extreplace(pvec, cg, x, h2);
                saving += k;
                if (k > 0) { std::cout << "L1 norm reduced by " << k << std::endl; }
            }
            totsaving += saving;
            std::cout << "Pass completed (subtotal = " << saving << ")." << std::endl;
        }

        std::cout << "Total saving: " << totsaving << std::endl;
        return cg;

    }

    cgsalvo<int64_t> improve(pattern initial, cgsalvo<int64_t> &cginit, int timelimit, int maxforward, int maxback,
                    std::map<std::string, std::set<std::string> >* sssmap, classifier &cfier,
                    std::map<std::pair<std::string, std::string>, int>* remwant) {

        cgsalvo<int64_t> cg = cginit;
        std::vector<pattern> pvec = regenerate(initial, cg);

        uint64_t i = 0;
        uint64_t imax = pvec.size();

        while ((i < pvec.size() - 1) && (i <= imax)) {
            std::cout << "\033[31;1m*** Glider " << i << " ***\033[0m" << std::endl;
            pcgpair pc = collmitm(pvec, i, timelimit, maxforward, maxback, sssmap, cfier, remwant);
            if (pc.second.gliders.size() > 0) {
                pattern newp(initial.getlab(), initial.getlab()->_string32(pc.first), initial.getrule());
                bool failed = true;
                for (uint64_t j = pvec.size() - 1; j > i; j--) {
                    // std::cout << j << std::endl;
                    if (newp == pvec[j]) {
                        cgsalvo<int64_t> cg2 = cg;
                        cg2.gliders.resize(i);
                        for (uint64_t k = 0; k < pc.second.gliders.size(); k++) {
                            cg2.gliders.push_back(pc.second.gliders[k]);
                        }
                        for (uint64_t k = j; k < cg.gliders.size(); k++) {
                            cg2.gliders.push_back(cg.gliders[k]);
                        }
                        std::cout << "\033[1;32m" <<  cg.gliders.size() << " --> " << cg2.gliders.size() << "\033[0m" << std::endl;
                        cg = cg2; failed = false;
                        break;
                    }
                }
                if (failed) {
                    std::cout << "Sanity check failed!" << std::endl;
                } else {
                    std::cout << "Regenerating..." << std::endl;
                    pvec = regenerate(initial, cg);
                }
            } else {
                i += 1;
            }
        }

        return cg;

    }

}

int oldmain() {

    apg::lifetree<uint32_t, 1> lt(1000);
    apg::classifier cfier(&lt, "b3s23");

    apg::pattern inf(&lt, "syringeloop.mc");
    int64_t bbox[4] = {0}; inf.getrect(bbox); inf = inf(-bbox[0], -bbox[1]);
    apg::pattern spat = inf & inf[2] & inf[4] & inf[6] & inf[8];

    apg::cgsalvo<int64_t> cg;
    cg.glidermatch(inf - spat);
    /*
    std::vector<apg::pattern> stages = apg::regenerate(spat, cg);
    apg::pattern fin = stages.back();
    int64_t bbox[4] = {0};
    fin.getrect(bbox); fin.display(bbox);
    */

    std::map<std::string, std::set<std::string> > sssmap;
    apg::encoll(apg::pattern(&lt, "ooo!", "b3s23"), 512, &sssmap);
    apg::encoll(apg::pattern(&lt, "oo$oo!", "b3s23"), 512, &sssmap);
    apg::encoll(apg::pattern(&lt, "o&o&o!", "b3s23"), 512, &sssmap);

    // apg::cgsalvo<int64_t> cs = apg::improve(spat, cg, 512, 3, 3, &sssmap, cfier, 0);

    apg::cgsalvo<int64_t> cs = apg::defragment(spat, cg, 10);
    cs = apg::improve(spat, cs, 512, 2, 2, &sssmap, cfier, 0);

    apg::pattern eglider(&lt, "3o$o$bo!", "b3s23");
    apg::pattern oglider(&lt, "b2o$2o$2bo!", "b3s23");
    apg::pattern slt = spat;
    for (uint64_t m = 0; m < cs.gliders.size(); m++) {
        std::pair<int64_t, char> ng = cs.gliders[m];
        int64_t posback = (m + 1) * 128;
        if (ng.second == 'E') {
            slt += eglider(posback + ng.first, posback);
        } else {
            slt += oglider(posback + ng.first, posback);
        }
    }
    slt.write_macrocell(std::cout);

    /*
    std::map<std::string, int> wanted;

    apg::pattern shoney(&lt, "43b2o$42bobo$36b2o4bo$34bo2bo2b2ob4o$34b2obobobobo2bo$37bobobobo$37bob"
"ob2o$38bo2$51b2o$42b2o7bo$42b2o5bobo$49b2o7$39b2o$40bo$37b3o$37bo4$9bo"
"$9b3o$12bo$11b2o3$3b2o42bo$3bo42bobo$2obo42bobo$o2b3o4b2o35bo$b2o3bo3b"
"2o$3b4o35b2o7b2o$3bo15b2o20bo2bo5bo2bo$4b3o12bobo20b2o7b2o$7bo13bo$2b"
"5o14b2o24bo$2bo43bobo$4bo41bobo$3b2o42bo!", "b3s23");

    apg::pattern snarks(&lt, "43b2o$42bobo$36b2o4bo$34bo2bo2b2ob4o$34b2obobobobo2bo$37bobobobo$37bob"
"ob2o$38bo2$51b2o$42b2o7bo$42b2o5bobo$49b2o7$39b2o$40bo$37b3o$37bo4$9bo"
"$9b3o$12bo$11b2o3$3b2o$3bo$2obo$o2b3o4b2o$b2o3bo3b2o$3b4o$3bo15b2o$4b"
"3o12bobo$7bo13bo$2b5o14b2o$2bo$4bo$3b2o!", "b3s23");

    apg::pattern sstart(&lt, "43b2o$42bobo$36b2o4bo$34bo2bo2b2ob4o$34b2obobobobo2bo$37bobobobo$37bob"
"ob2o$38bo2$51b2o$42b2o7bo$42b2o5bobo$49b2o4$18b2o$18b2o2$39b2o$40bo$"
"37b3o$37bo7bo$44bobo$43bo2bo$44b2o$9bo$9b3o$12bo$11b2o28b3o2$45bo$3b2o"
"40bo$3bo41bo$2obo36bo$o2b3o4b2o27bobo$b2o3bo3b2o27bobo$3b4o33bo$3bo15b"
"2o$4b3o12bobo$7bo13bo$2b5o14b2o$2bo$4bo$3b2o!", "b3s23");

    std::map<std::string, std::set<std::string> > sssmap;
    std::set<std::string> output;

    apg::encoll(apg::pattern(&lt, "oo$oo!", "b3s23"), 512, &sssmap);

    apg::tellmewhatyouwant(sstart, shoney, snarks, 512, 18, 4, &sssmap, &wanted, cfier);

    apg::pcgpair pcg = apg::colltree(sstart, snarks, 512, &wanted, 3);
    std::cout << pcg.second.gliders.size() << std::endl;    

    apg::pattern eglider(&lt, "3o$o$bo!", "b3s23");
    apg::pattern oglider(&lt, "b2o$2o$2bo!", "b3s23");
    apg::cgsalvo<int64_t> cs = pcg.second;
    apg::pattern slt = sstart;
    for (uint64_t m = 0; m < cs.gliders.size(); m++) {
        std::pair<int64_t, char> ng = cs.gliders[m];
        int64_t posback = (m + 1) * 128;
        if (ng.second == 'E') {
            slt += eglider(posback + ng.first, posback);
        } else {
            slt += oglider(posback + ng.first, posback);
        }
    }

    slt.write_macrocell(std::cout);
    */

    return 0;

}
