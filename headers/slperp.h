#pragma once

#include "../lifelib/pattern2.h"
#include "../lifelib/ssplit.h"
#include <fstream>

namespace apg {

    typedef std::pair<int64_t, std::pair<char, char> > lanespec;

    struct scstream {

        std::vector<int64_t> gstream;
        std::vector<int64_t> endblocks;
        std::vector<lanespec> gouts;

        int64_t initpos;
        int64_t delay;

        scstream() {
            delay = 0;
            initpos = 0;
            gstream.push_back(0);
            endblocks.push_back(0);
        }

        void exportgliders(std::ostream &os) {
            for (uint64_t i = 0; i < gstream.size(); i++) {
                if (i == 0) {
                    os << "[0";
                } else {
                    os << gstream[i];
                }
                if (i == gstream.size() - 1) {
                    os << "]" << std::endl;
                } else {
                    os << ((i % 16 == 15) ? ",\n" : ", ");
                }
            }
        }

        std::string tostring() {

            std::ostringstream os;
            os << '[';
            for (uint64_t i = 0; i < gstream.size(); i++) {
                os << gstream[i] << ((i == gstream.size() - 1) ? "]" : ", ");
            }
            for (uint64_t i = 0; i < gouts.size(); i++) {
                os << ' ' << gouts[i].first << gouts[i].second.first << gouts[i].second.second;
            }
            for (uint64_t i = 0; i < endblocks.size(); i++) {
                os << ' ' << ((endblocks[i] & 1) ? '7' : '0') << "move" << endblocks[i];
            }
            return os.str();

        }

        scstream(std::string dline) {
            delay = 0;
            initpos = 0;
            size_t h = dline.find(']');
            if (h != std::string::npos) {
                std::istringstream in(dline.substr(0, h));
                std::string annotations = dline.substr(h);
                onlyints(gstream, in);
                for (uint64_t i = 0; i < gstream.size(); i++) {
                    delay += gstream[i];
                }

                std::vector<std::string> things = string_split(annotations, ' ');
                for (uint64_t i = 0; i < things.size(); i++) {
                    std::string thing = things[i];
                    if (thing.size() >= 3) {
                        if ((thing[thing.size() - 1] == 'E') || (thing[thing.size() - 1] == 'O')) {
                            int64_t glane = std::stoll(thing.substr(0, thing.size() - 2));
                            std::pair<char, char> gmisc(thing[thing.size() - 2], thing[thing.size() - 1]);
                            gouts.emplace_back(glane, gmisc);
                        } else if (thing[1] == 'm') {
                            endblocks.push_back(std::stoll(thing.substr(5)));
                        }
                    }
                }
            }
        }

        void translate(int64_t offset) {
            for (uint64_t i = 0; i < endblocks.size(); i++) {
                endblocks[i] += offset;
            }
            for (uint64_t i = 0; i < gouts.size(); i++) {
                if ((gouts[i].second.first == 'x') || (gouts[i].second.first == 'i')) {
                    gouts[i].first += offset;
                    if (offset & 1) { gouts[i].second.first ^= ('x' ^ 'i'); }
                } else if (offset & 1) {
                    gouts[i].first = 0 - gouts[i].first;
                }
            }
            initpos += offset;
        }

        void concat(scstream &other, uint32_t parity) {
            // parity = 0: don't care;
            // parity = 1: odd start position;
            // parity = 2: even start position.

            // Ensure delay is congruent to parity:
            if ((parity != 0) && ((delay ^ parity) & 1)) { delay += 1; gstream[gstream.size() - 1] += 1; }

            // Modify final block:
            if (other.gstream.size() == 0) { return; }
            scstream x = other;
            x.translate(endblocks.back() - x.initpos);
            endblocks.pop_back();

            for (uint64_t i = 0; i < x.gouts.size(); i++) {
                if (delay & 1) { x.gouts[i].second.second ^= ('E' ^ 'O'); }
                gouts.push_back(x.gouts[i]);
            }

            for (uint64_t i = 0; i < x.endblocks.size(); i++) {
                endblocks.push_back(x.endblocks[i]);
            }

            for (uint64_t i = 0; i < x.gstream.size(); i++) {
                if (i == 0) {
                    gstream[gstream.size() - 1] += x.gstream[i];
                } else {
                    gstream.push_back(x.gstream[i]);
                }
                delay += x.gstream[i];
            }
        }

        void close_paren() {

            scstream scs2("[0, 109, 90, 93, 91, 90, 90, 90, 90, 121, 91, 91, 90, 256] paren-right");
            concat(scs2, 2);

        }

        void open_paren(int64_t fd) {

            scstream scs2("[0, 109, 90, 93, 91, 90, 95, 91, 90, 91, 91, 90, 90, 91, 90, 90, 99, 90, 90, 91, 90, 94, 90, 90, 109, 91, 94, 91, 91, 189, 91, 90, 91, 292, 90, 91, 158, 91, 90, 90, 90, 91, 116, 91, 137, 91, 90, 91, 90, 90, 149, 200, 90, 90, 154, 90, 91, 256] 0move0 0move-144");

            for (uint64_t i = 0; i < scs2.gstream.size(); i++) {
                if (scs2.gstream[i] == 292) {
                    scs2.gstream[i] += ((fd - 72) * 8);
                }
            }

            scs2.endblocks[1] = (-2 * fd);
            concat(scs2, 2);
        }

    };

    struct elbowlab {

        std::string corderpush2;
        std::string crabpush;

        std::string snarkmaker;
        std::vector<scstream> emitters;
        std::vector<scstream> movers;
        std::map<std::pair<char, int64_t>, scstream> emmap;
        std::map<int64_t, std::vector<scstream> > zerodeg;

        std::string loadpush(std::string filename) {

            std::string corderpush;
            std::ifstream ins(filename);
            std::string item;
            while (std::getline(ins, item, '\n')) {
                corderpush += " " + item;
            }
            return corderpush;
        }

        void loadsm(std::string filename) {

            std::string corderpush;
            std::ifstream ins(filename);
            std::string item;
            while (std::getline(ins, item, '\n')) {
                corderpush += " " + item;
            }
            snarkmaker = corderpush;
            std::cerr << "Snarkmaker string = " << snarkmaker.length() << " bytes." << std::endl;
        }

        void load0deg(std::string filename) {

            std::ifstream infile(filename);
            std::string line;

            while (std::getline(infile, line)) {
                scstream scs(line);
                zerodeg[scs.gouts[0].first].push_back(scs);
                scstream scs2("[0, 109, 90, 93, 91, 90, 95, 91, 91, 138, 157, 96, 90, 120, 91, 97, 107, 90, 90, 93, 188] 0move44");
                scs2.concat(scs, 0);
                zerodeg[scs.gouts[0].first].push_back(scs2);

                scstream scs3("[0, 109, 91, 94, 91, 90, 96, 90, 91, 146, 240] 7move-1");
                scs3.concat(scs, 0);
                zerodeg[-scs.gouts[0].first].push_back(scs3);
            }

        }

        void loadrecs(std::string filename) {

            scstream scempty;
            movers.push_back(scempty);
            std::ifstream infile(filename);
            std::string line;

            while (std::getline(infile, line)) {
                scstream scs(line);
                if (scs.endblocks.size() == 1) {
                    if (scs.gouts.size() == 1) { emitters.push_back(scs); }
                    if (scs.gouts.size() == 0) { movers.push_back(scs); }
                }
            }

            // Find minimal recipes for glider emitters:
            for (uint64_t j = 0; j < movers.size(); j++) {
                for (uint64_t i = 0; i < emitters.size(); i++) {
                    scstream a = movers[j];
                    a.concat(emitters[i], 0);
                    std::pair<char, int64_t> pp(a.gouts[0].second.first, a.gouts[0].first);
                    auto it = emmap.find(pp);
                    if ((it == emmap.end()) || (it->second.delay > a.delay)) {
                        emmap.insert(std::pair<std::pair<char, int64_t>, scstream>(pp, a));
                    }
                }
            }
        }

        scstream approximate(int64_t rel) {

            if ((rel > 236000) && (crabpush.length() > 0)) {

                scstream scs2(crabpush);

                for (uint64_t i = 0; i < scs2.gstream.size(); i++) {
                    if (scs2.gstream[i] == 1029) {
                        scs2.gstream[i] += (((rel - 66000) / 24) * 12);
                    }
                }

                scs2.endblocks[0] += ((rel - 66000) / 24) * 24;

                scstream scs3 = movers[1];
                for (uint64_t i = 0; i < 8; i++) { scs3.concat(movers[1], 0); }
                scs3.concat(scs2, 0);

                return scs3;

            } else if ((rel > 4000) && (corderpush2.length() > 0)) {

                scstream scs2(corderpush2);

                for (uint64_t i = 0; i < scs2.gstream.size(); i++) {
                    if (scs2.gstream[i] == 12922) {
                        scs2.gstream[i] += (((rel - 4000) / 16) * 64);
                    }
                }

                scs2.endblocks[0] += ((rel - 4000) / 16) * 16;

                scstream scs3 = movers[1];
                for (uint64_t i = 0; i < 8; i++) { scs3.concat(movers[1], 0); }
                scs3.concat(scs2, 0);

                return scs3;

            } else if (rel < -400) {

                std::string puller = "[0, 109, 90, 93, 91, 91, 90, 90, 100, 90, 90, 146, 92, 90, 90, 103, ";
                puller += "90, 91, 91, 157, 603, 90, 157, 90, 138, 91, 90, 90, 92, 91, 147, ";
                puller += "90, 91, 90, 165, 129, 90, 109, 91, 93, 90, 140, 150, 113, 90, 90, ";
                puller += "90, 90, 91, 136, 119, 127, 90, 103, 91, 99, 116] 0move-170";

                scstream scs2(puller);

                for (uint64_t i = 0; i < scs2.gstream.size(); i++) {
                    if (scs2.gstream[i] == 603) {
                        scs2.gstream[i] += (((-rel - 200) / 16) * 64);
                    }
                }

                scs2.endblocks[0] -= ((-rel - 200) / 16) * 16;
                scstream scs3 = movers[2];
                scs3.concat(scs2, 0);

                return scs3;

            } else {

                return movers[(rel > 0) ? 1 : 2];

            }

        }

        bool desipos(scstream &scs, int64_t desired) {

            int64_t rel = desired - scs.endblocks.back();
            for (uint64_t i = 0; i < movers.size(); i++) {
                if (movers[i].endblocks.back() == rel) {
                    scs.concat(movers[i], 0);
                    return true;
                }
            }
            scstream gls = approximate(rel);
            scs.concat(gls, 0);
            return false;

        }

        bool desirast(scstream &scs, lanespec desired) {

            lanespec rel = desired;
            if (scs.endblocks.back() & 1) { rel.second.first ^= ('x' ^ 'i'); }
            rel.first -= scs.endblocks.back();

            std::pair<char, int64_t> pp(rel.second.first, rel.first);
            auto it = emmap.find(pp);
            if (it == emmap.end()) {
                scstream gls = approximate(rel.first);
                scs.concat(gls, 0);
                return false;
            } else {
                scstream gls = it->second;
                uint32_t parity = (rel.second.second == gls.gouts.back().second.second) ? 2 : 1;
                scs.concat(gls, parity);
                return true;
            }

        }

        bool fire_parallel(scstream &scs, lanespec desired, int64_t origback) {

            lanespec rel = desired;
            if (scs.endblocks.back() & 1) { rel.first *= -1; }

            auto it = zerodeg.find(rel.first);
            if ((it == zerodeg.end()) || (it->second.size() == 0)) {
                return false;
            } else {

                int64_t bestcost = 1000000000;
                scstream gls;
                for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
                    int64_t newcost = 400 * it2->gstream.size(); // and now add the change in quadratic penalty:
                    newcost += (2 * (scs.endblocks.back() - origback) + it2->endblocks.back()) * it2->endblocks.back();
                    if (newcost < bestcost) {
                        bestcost = newcost;
                        gls = (*it2);
                    }
                }

                uint32_t parity = (rel.second.second == gls.gouts.back().second.second) ? 2 : 1;
                scs.concat(gls, parity);
                return true;
            }

        }

        void desirate(scstream &scs, lanespec desired) {
            while (!desirast(scs, desired)) { }
        }

        void desirate(scstream &scs, int64_t desired) {
            while (!desipos(scs, desired)) { }
        }

        scstream break_snark(int64_t fdpush) {

            scstream scs2("[0, 93, 91, 118, 93, 151, 90, 99, 155, 120, 92, 108, 90, 102, 164, 90, 96, 887, 179, 97, 90, 109, 91, 93, 91, 92, 90, 90, 90, 151, 93, 90, 143, 134, 94, 90, 90, 90, 109, 91, 90] 0move0");

            for (uint64_t i = 0; i < scs2.gstream.size(); i++) {
                if (scs2.gstream[i] == 887) {
                    scs2.gstream[i] += ((fdpush - 128) * 8);
                }
            }

            return scs2;

        }

        scstream make_snark(int64_t fdpush) {

            scstream scs2(snarkmaker);
            desirate(scs2, (fdpush - 128) * 2);
            scs2.endblocks[0] = 0;
            return scs2;

        }

    };

}

