#include "headers/slsparse.h"

///bin/cat /dev/null; bash update-lifelib.sh
///bin/cat /dev/null; echo 'Compiling...'
///bin/cat /dev/null; sourcename="$0"
///bin/cat /dev/null; exename="$( readlink -f "${sourcename%.*}" )"
///bin/cat /dev/null; g++ -std=c++11 -O3 -march=native -pedantic -Wall -Wextra "$sourcename" -o "$exename"
///bin/cat /dev/null; echo '...compiled.'
///bin/cat /dev/null; date
///bin/cat /dev/null; "$exename" "$@"; status=$?
///bin/cat /dev/null; date
///bin/cat /dev/null; exit $status

int main(int argc, char* argv[]) {

    std::string infile  =  "infile.mc";
    std::string outfile = "outfile.mc";

    if (argc > 1) {  infile = argv[1]; }
    if (argc > 2) { outfile = argv[2]; }

    apg::lifetree<uint32_t, 1> lt(1000);
    apg::lifetree<uint32_t, 4> lt4(100);
    apg::pattern inf(&lt4, infile);
    apg::sparselab spl(&lt);

    apg::scstream scs;

    bool success = spl.arbitrary_build(inf, scs);

    if (success == false) {
        std::cerr << "Error: no initial block-on-mango specified." << std::endl;
        return 1;
    }

    std::ofstream f("outfile.txt");
    scs.exportgliders(f);

    apg::pattern x(&lt, "", "b3s23");
    x += spl.target_to_glider(inf);

    apg::pattern y = x & x[2] & x[4] & x[6] & x[8];
    apg::pattern g = x - y;
    y += g.stream(scs.gstream);
    std::ofstream out(outfile);
    y.write_macrocell(out);

    return 0;

}
