#!/usr/bin/python

import lifelib

print("Lifelib version: %s" % lifelib.__version__)

from sys import argv

def main():

    infile  =  'infile.mc' if (len(argv) <= 1) else argv[1]
    outfile = 'outfile.mc' if (len(argv) <= 2) else argv[2]

    sess = lifelib.load_rules('b3s23', force_indirect=True)
    lt = sess.lifetree(n_layers=1)

    def run_to_completion(pat):

        bb = pat.bounding_box
        x = 4 * (bb[2] + bb[3])
        x -= (x % 4096)
        x += 6144

        return pat[x]

    print("Loading patterns...")
    a = lt.load(infile)
    b = lt.load(outfile)

    print("Replacing objects...")

    # Replace block-on-mango with block:
    findme = lt.pattern("6$4bo$3bobob2o$2bo2bob2o$2bobo$3bo!", "b3s23")
    replaceme = lt.pattern("7$7b2o$7b2o!", "b3s23")
    a = a.replace(findme, replaceme, orientations='rotate4reflect')

    # Replace PS4B with empty space:
    findme = lt.pattern("3b2o$2bo2bo$bob2obo$obo2bobo$obo2bobo$bob2obo$2bo2bo$3b2o!", "b3s23")
    replaceme = lt.pattern("", "b3s23")
    a = a.replace(findme, replaceme, orientations='rotate4reflect')

    print("Running to completion...")
    a = run_to_completion(a)
    b = run_to_completion(b)

    print("Populations and bounding boxes:")
    print(a.population)
    print(b.population)
    print(a.bounding_box)
    print(b.bounding_box)

    if (a.population != b.population):
        raise ValueError("Populations do not match")

    if (a.bounding_box != b.bounding_box):
        raise ValueError("Bounding boxes do not match")

    if (a != b):
        raise ValueError("Patterns do not match")

    print("Input and output files are consistent")

if __name__ == '__main__':

    main()
