#include <stdint.h>
#include <iostream>


int counique(uint8_t* currstack) {

    uint64_t backdrop[49] = {};

    for (int i = 0; i < 512; i++) {

        uint8_t a = currstack[(i & 3) | ((i >> 1) & 12)];
        uint8_t b = currstack[((i >> 1) & 3) | ((i >> 2) & 12)];
        uint8_t c = currstack[((i >> 3) & 3) | ((i >> 4) & 12)];
        uint8_t d = currstack[((i >> 4) & 3) | ((i >> 5) & 12)];

        backdrop[7*a + b] |= (1ull << (7*c + d));

    }

    int p = 0;

    for (int i = 0; i < 49; i++) {
        p += __builtin_popcountll(backdrop[i]);
    }

    return p;

}


void cubewalk() {

    uint8_t currstack[16];
    uint8_t maxstack[16];

    currstack[0] = 0;
    currstack[1] = 1;
    maxstack[0] = 0;

    int visited = 0;
    int visited2[] = {0, 0, 0, 0, 0, 0, 0, 0};
    int focus = 1;
    while (focus) {

        uint8_t limit = maxstack[focus - 1] + 1;
        limit = (limit > 6) ? 6 : limit;
        if (currstack[focus] > limit) {
            focus -= 1;
            currstack[focus] += 1;
        } else {

            bool invalid = false;

            invalid = invalid || ((focus & 1) && (currstack[focus] == currstack[focus - 1]));
            invalid = invalid || ((focus & 2) && (currstack[focus] == currstack[focus - 2]));
            invalid = invalid || ((focus & 4) && (currstack[focus] == currstack[focus - 4]));
            invalid = invalid || ((focus & 8) && (currstack[focus] == currstack[focus - 8]));

            if (invalid) {
                currstack[focus] += 1;
            } else {
                maxstack[focus] = maxstack[focus - 1];
                if (maxstack[focus] < currstack[focus]) { maxstack[focus] = currstack[focus]; }
                if (focus < 15) {
                    focus += 1;
                    currstack[focus] = 0;
                } else {
                    visited += 1;
                    visited2[maxstack[focus]] += 1;
                    if (visited % 1000000 == 0) { std::cerr << visited << std::endl; }

                    if (maxstack[focus] == 6) {
                        // We have a 7-colouring which uses all 7 colours:
                        int q = counique(currstack);
                        if (q == 512) {
                            for (int i = 0; i < 16; i++) {
                                std::cout << ((int) currstack[i]) << ' ';
                            }
                            std::cout << std::endl;
                        }
                    }

                    currstack[focus] += 1;
                }
            }
        }
    }

    std::cerr << visited << " distinct colourings encountered." << std::endl;
    for (int i = 0; i < 8; i++) {
        std::cerr << visited2[i] << std::endl;
    }
}



int main() {

    cubewalk();
    return 0;

}
